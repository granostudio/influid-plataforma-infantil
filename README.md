# Diario de Aprendizagem
*Este Projeto foi construido utilizando [Parcel Bundler](https://parceljs.org/), Para consulta utilizar este [site](https://parceljs.org/getting_started.html)*

Para realizar a execução deste projeto é necessária a instalação do [npm](https://www.npmjs.com/) que pode ser realizada do seguinte modo:(** este comando deve ser realizado dentro do diretório do projeto **) 
```
npm install
```

### Realizar o build do Projeto
(Após instalar o npm e as dependencias do projeto)
```
npm run build
```
este comando produzirá uma pasta com o nome `build` onde estarão os arquivos gerados
