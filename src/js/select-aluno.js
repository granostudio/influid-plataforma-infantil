$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    // $styledSelect.text($this.children('option').eq(0).text());
    $styledSelect.append('<span class="texto-select-aluno"></span>');
    var $selectAluno = $('.texto-select-aluno');
    $selectAluno.text($this.children('option').eq(0).text());

    $styledSelect.append('<span class="ti-angle-up"></span>');
    
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');

    

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').slideToggle();
        });
        $(this).toggleClass('active').next('ul.select-options').slideToggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.removeClass('active');
        $selectAluno.text($(this).text())

        // $styledSelect.append('<span class="ti-angle-up"></span>');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

    // for (let v = 2; v < numberOfOptions; v++) {
    // //    console.log('nth-child(' + v + ')');
    //    $('.select-options li:nth-child(' + v + ')').addClass('aluno-item');
    // //    console.log('teste');
    // }

    $('.select-options li').addClass('aluno-item');

});