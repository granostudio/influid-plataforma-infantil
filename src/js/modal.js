// --------- CONTAGEM DE CARACTERES TEXTAREA ---------
var maxLength = 500;
$('textarea').keyup(function() {
  var textlen = maxLength - $(this).val().length;
  $('#contText').text(textlen);
});
// ---------------------------------------------------

// --------- FUNCIONAMENTO DO BOTAO DE ANEXO ------------

// You can modify the upload files to pdf's, docs etc
//Currently it will upload only images

$(document).ready(function($) {

    // Upload btn on change call function
    $(".uploadlogo").change(function() {
      var filename = readURL(this);
      $(this).parent().children('.textInput').html(filename);
    });
  
    // Read File and return value  
    function readURL(input) {
      var url = input.value;
      var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
      if (input.files && input.files[0] && (
        ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "gif" || ext == "pdf"
        )) {
        var path = $(input).val();
        var filename = path.replace(/^.*\\/, "");
        // $('.fileUpload span').html('Uploaded Proof : ' + filename);
        $(".fileUpload").addClass("anexado");
        $(".fileUpload").removeClass("blue-btn");
        $(".pe-7s-paperclip").remove();
        // ---- adiciona nome do arquivo anexado ----
        // return "Uploaded file : "+filename;
        return "ARQUIVOS ANEXADOS";
      } else {
        $(input).val("");
        return "Arquivo não suportado!";
      }
    }
    // Upload btn end
  
  });

//   ------------ EXIBICAO BOX EXCLUIR -----------
// $('.lista-excluir').click(function(){
//     // console.log('click!!!');

//     $('.modal-excluir').fadeToggle();
//     $('.btn-voltar').click(function(){
//         $('.modal-excluir').fadeOut();
//     });
//     $('.btn-excluir').click(function(){
//         $(this).closest('.modal-coment-item').remove();
//     });
// });


// teste
$('.lista-excluir').on('click', function(){
  // console.log('click!!!');
  let modalExcluir = $(this).parent('div').parent('div').next('.modal-excluir');
  modalExcluir.toggleClass('ativo');

  if(modalExcluir.hasClass('ativo')) {
    // modalExcluir.fadeToggle();
    
    let btnVoltar = modalExcluir.find('.btn-voltar');
    btnVoltar.click(function(){
      modalExcluir.removeClass('ativo');
      // modalExcluir.fadeOut();
    });
  
    let btnExcluir = modalExcluir.find('.btn-excluir');
    btnExcluir.click(function(){
      let teste = modalExcluir.parent('.modal-coment-item');
      teste.remove();
    });
  }
  
  // $('.modal-excluir').fadeToggle();
  // $('.btn-voltar').click(function(){
  //     $('.modal-excluir').fadeOut();
  // });
  // $('.btn-excluir').click(function(){
  //     $(this).closest('.modal-coment-item').remove();
  // });
});


// teste de btn de anexo
let btnItemAnexo = $('.edt');

let excluiAnexo = btnItemAnexo.find('.anexo-item-remove');

btnItemAnexo.find('.anexo-item-remove').click(function(){
  $(this).parent('.edt').remove();
});

// excluiAnexo.click(function(){
//   // console.log('click no "x"');
//   excluiAnexo.parent('.edt').remove();
// });
