$( document ).ready(function(){
    // APRESENTA O PRIMEIRO ITEM AO CARREGAR A PAGINA
    $('.sala-item').first().addClass('sala-ativa');
    // CARACTERIZA O PRIMEIRO BOTAO COMO ATIVO
    $('.turmas-item').first().addClass("item-ativo");
    $('.bloco-ativo').css("top",0);
    $('.item-ativo .turmas-item-text').first().addClass("turmas-item-ativo-text");
    // MUDA O BOTAO ATIVO ATRAVES DO CLIQUE E REMOVE A CLASSE ATIVA DO PRIMEIRO ITEM
    $('.turmas-item:nth-child(n)').click(function(){
        if($(this).hasClass('item-ativo')) {

        }
        else {
            $('.sala-item').removeClass('sala-ativa');
            $('.item-ativo .turmas-item-text').removeClass("turmas-item-ativo-text");
            $('.turmas-item').removeClass("item-ativo");
            let numItem = ($(this).index()+1);
            // console.log(numItem);
            $('.sala-item:nth-child(' + numItem + ')').addClass('sala-ativa');
            $('.bloco-ativo').css("top", ((numItem - 1) * 50));
            $('.turmas-item:nth-child(' + numItem + ')').addClass('item-ativo');
            $('.turmas-item:nth-child(' + numItem + ') .turmas-item-text').addClass('turmas-item-ativo-text');
        }
    });
});