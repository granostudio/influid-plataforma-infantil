$( document ).ready(function(){
    // APRESENTA O PRIMEIRO ITEM AO CARREGAR A PAGINA
    $('.sala-item').first().addClass('sala-ativa');
    // CARACTERIZA O PRIMEIRO BOTAO COMO ATIVO
    $('.aluno-item').first().addClass("aluno-ativo");
    $('.aluno-ativo .aluno-item-text').first().addClass("aluno-aluno-ativo-text");
    // MUDA O BOTAO ATIVO ATRAVES DO CLIQUE E REMOVE A CLASSE ATIVA DO PRIMEIRO ITEM
    $('.aluno-item:nth-child(n)').click(function(){
        if($(this).hasClass('aluno-ativo')) {

        }
        else {
            $('.sala-item').removeClass('sala-ativa');
            $('.aluno-ativo .aluno-item-text').removeClass("aluno-aluno-ativo-text");
            $('.aluno-item').removeClass("aluno-ativo");
            let numItem = ($(this).index() + 1);
            // console.log(numItem);
            $('.sala-item:nth-child(' + numItem + ')').addClass('sala-ativa');
            $('.aluno-item:nth-child(' + numItem + ')').addClass('aluno-ativo');
            $('.aluno-item:nth-child(' + numItem + ') .aluno-item-text').addClass('aluno-aluno-ativo-text');
        }
    });
});